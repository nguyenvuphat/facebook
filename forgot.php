<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Facebook Login Page</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="login-wrapper">
    <div class="login-wrapper__header">
        <div class="container header-box">
            <a href="#" class="login-brand">facebook</a>
            <div class="login-form">
                <div class="login-form__email">
                    <label class="title">Email or phone</label>
                    <input class="input-login" name="username" type="email" id="username">
                </div>
                <div class="login-form__password">
                    <label class="title">Password</label>
                    <input class="input-login" name="password" type="password" id="password">
                    <a href="#" class="forgot-password-link">Forgotten account?</a>
                </div>
                <input type="submit" class="btn-login" value="Login" onclick="login()">

            </div>
        </div>
    </div>
    <div class="login-wrapper__content forgot">
        <div class="forgot-box">
            <h2 class="forgot-box__title">Recover your password</h2>
            <hr>
            <div class="forgot-box__input">
                <span class="forgot-box__description">Please enter your email or phone number to search for your account.</span>
                <input id="forgot-box__text" class="forgot-box__text" type="text">
                <button class="btn-recover" onclick="recoverPassword()">Recover</button>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</body>

<script>
    function recoverPassword() {
        var username = $("#forgot-box__text").val();

        $.ajax({
            url: "controller/UserController.php",
            method: "POST",
            data: {
                action: "recoverPassword",
                username: username
            },
            success: function (result) {
                if (result.includes("Message has been sent")) {
                    alert("New password has been sent to your mailbox");
                }
            }
        })
    }
</script>

</html>