<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require ("../plugins/src/Exception.php");
require ("../plugins/src/PHPMailer.php");
require ("../plugins/src/SMTP.php");

require_once("database.php");

session_start();

$imageDefaultPath = "uploads/default.jpg";

if (isset($_POST["firstName"])) {
    $firstName = trim($_POST["firstName"]);
}

if (isset($_POST["lastName"])) {
    $lastName = trim($_POST["lastName"]);
}

if (isset($_POST["username"])) {
    $email = trim($_POST["username"]);
}

if (isset($_POST["password"])) {
    $password = trim($_POST["password"]);
}

if (isset($_POST["gender"])) {
    $gender = trim($_POST["gender"]);
}

if (isset($_POST["birthDay"])) {
    $birthDay = trim($_POST["birthDay"]);
}

if (isset($_POST['action'])) {
    $action = $_POST['action'];
}

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

switch ($action) {
    case 'add':
    {
        $query = $dbconn->prepare("INSERT INTO users(user_firstname, user_lastname, user_email, 
                                                user_password, user_image, user_gender, user_birthday)
                                                VALUES (:user_firstname, :user_lastname, :user_email, 
                                                :user_password, :user_image, :user_gender, :user_birthday)");
        $query->bindParam(':user_firstname', $firstName);
        $query->bindParam(':user_lastname', $lastName);
        $query->bindParam(':user_email', $email);
        $query->bindParam(':user_password', $password);
        $query->bindParam(':user_image', $imageDefaultPath);
        $query->bindParam(':user_gender', $gender);
        $query->bindParam(':user_birthday', $birthDay);
        // insert a row
        if ($query->execute()) {
            $result = 1;
        }

        echo $result;

        break;
    }
    case "login":
    {
        $query = $dbconn->prepare("select id, count(*) as count from users
                                            where user_email=:email and user_password=:password");

        $query->bindParam(':email', $email);
        $query->bindParam(':password', $password);

        $query->execute();

        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $user_count = $row["count"];

            $user_id = $row["id"];
        }

        if ($user_count == 1) {
            $_SESSION["email"] = $email;
            $_SESSION["userId"] = $user_id;

            echo "Login successfully";
        } else {
            echo "Username and Password is incorrect";
        }
        break;
    }

    case "getDetail":
    {
        $email = $_GET["email"];

        $query = $dbconn->prepare("select * from users
                                            where user_email=:email");

        $query->bindParam(':email', $email);
        $query->execute();

        $userDetail = $query->fetch(PDO::FETCH_ASSOC);

        echo json_encode($userDetail);
        break;
    }
    case "addPost":
    {
        if ($_POST["post_message"]) {
            $post_message = $_POST["post_message"];
        }

        if ($_POST["post_image_path"]) {
            $post_image_path = $_POST["post_image_path"];
        }

        if ($_POST["userId"]) {
            $poster = $_POST["userId"];
            $poster = (int)$poster;
        }

        $created = @date('Y-m-d H:i:s');

        $query = $dbconn->prepare("INSERT INTO posts(post_message, post_image, poster, created)
                                                VALUES (:post_message, :post_image, :poster, :created)");

        $query->bindParam(':post_message', $post_message);
        $query->bindParam(':post_image', $post_image_path);
        $query->bindParam(':poster', $poster);
        $query->bindParam(':created', $created);

        if ($query->execute()) {
            $arr = array(
                'post_message' => $post_message,
                'post_image' => $post_image_path,
                'poster' => $poster,
                'created' => $created,
                'isSuccess' => true
            );

            echo json_encode($arr);
        } else {
            $arr = array(
                'post_message' => $post_message,
                'post_image' => $post_image_path,
                'poster' => $poster,
                'created' => $created,
                'isSuccess' => false,
                'message' => $query->errorInfo()
            );
            echo json_encode($arr);;
        }
        break;
    }

    case "retrievePost":
    {
        if ($_GET["userId"]) {
            $poster = (int)$_GET["userId"];
        }
        $query = $dbconn->prepare("select * from posts p
                                            join users u on p.poster = u.id
                                            where p.poster=:userId
                                            order by p.created desc");

        $query->bindParam(':userId', $poster);

        if ($query->execute()) {
            echo json_encode($query->fetchAll());
        } else {
            echo $query->errorInfo();
        }
        break;
    }
    case "retrieveUserByTextSearch":
    {
        if ($_GET["textSearch"]) {
            $textSearch = $_GET["textSearch"];
        }

        $query = $dbconn->prepare("select * from users
                                            where user_firstname like ? or user_lastname like ?");

        $params = array('%' . $textSearch . '%', '%' . $textSearch . '%');

        if ($query->execute($params)) {
            echo json_encode($query->fetchAll());
        } else {
            echo $query->errorInfo();
        }

        break;
    }
    case "requestFriend": {
        if (isset($_POST["sender"])) {
            $sender = $_POST["sender"];
        }

        if (isset($_POST["receiver"])) {
            $receiver = $_POST["receiver"];
        }

        $query = $dbconn->prepare("INSERT INTO friend_requests(sender, receiver)
                                                VALUES (:sender, :receiver)");

        $query->bindParam(':sender', $sender);
        $query->bindParam(':receiver', $receiver);

        if ($query->execute()) {
            $arr = array(
                'isSuccess' => true
            );

        } else {
            $arr = array(
                'isSuccess' => false,
                'message' => $query->errorInfo()
            );
        }

        echo json_encode($arr);

        break;
    }
    case "retrieveListRequestFriend": {
        if ($_GET["userId"]) {
            $userId = $_GET["userId"];
        }

        $query = $dbconn->prepare("select u.id, u.user_firstname, u.user_lastname, u.user_email,
                                            u.user_image 
                                            from friend_requests f JOIN users u on f.sender = u.id
                                            where receiver=:userId");

        $query->bindParam(':userId', $userId);

        if ($query->execute()) {
            echo json_encode($query->fetchAll());
        } else {
            echo $query->errorInfo();
        }

        break;
    }
    case "addFriend": {
        if (isset($_POST["currentUserId"])) {
            $currentUserId = $_POST["currentUserId"];
        }

        if (isset($_POST["acceptedFriendId"])) {
            $acceptedFriendId = $_POST["acceptedFriendId"];
        }

        // user one add friend user two
        $queryUserOneAddUserTwo = $dbconn->prepare("INSERT INTO friends(userOne, userTwo)
                                                VALUES (:userOne, :userTwo)");

        $queryUserOneAddUserTwo->bindParam(':userOne', $currentUserId);
        $queryUserOneAddUserTwo->bindParam(':userTwo', $acceptedFriendId);

        // user two add friend user one
        $queryUserTwoAddUserOne = $dbconn->prepare("INSERT INTO friends(userOne, userTwo)
                                                VALUES (:userOne, :userTwo)");

        $queryUserTwoAddUserOne->bindParam(':userOne', $acceptedFriendId);
        $queryUserTwoAddUserOne->bindParam(':userTwo', $currentUserId);

        if ($queryUserOneAddUserTwo->execute() && $queryUserTwoAddUserOne->execute()) {
            $arr = array(
                'isSuccess' => true
            );

        } else {
            $arr = array(
                'isSuccess' => false,
                'message' => $query->errorInfo()
            );
        }

        echo json_encode($arr);

        break;
    }
    case "deleteFriendRequest": {
        if (isset($_POST["sender"])) {
            $sender = $_POST["sender"];
        }

        if (isset($_POST["receiver"])) {
            $receiver = $_POST["receiver"];
        }

        $query = $dbconn->prepare("DELETE FROM friend_requests
                                                where sender=:sender and receiver=:receiver");

        $query->bindParam(':sender', $sender);
        $query->bindParam(':receiver', $receiver);

        if ($query->execute()) {

            $arr = array(
                'isSuccess' => true
            );

        } else {
            $arr = array(
                'isSuccess' => false,
                'message' => $query->errorInfo()
            );
        }

        echo json_encode($arr);

        break;
    }
    case "retrieveListFriend": {
        if ($_GET["userId"]) {
            $userId = $_GET["userId"];
        }

        $query = $dbconn->prepare("select * from users 
                                            where id in (
                                                select userTwo from friends where userOne=:userId
                                            )"
                                 );

        $query->bindParam(':userId', $userId);

        if ($query->execute()) {
            echo json_encode($query->fetchAll());
        } else {
            echo $query->errorInfo();
        }
        break;
    }
    case "sendMessage": {
        if (isset($_POST["user_from"])) {
            $user_from = $_POST["user_from"];
        }

        if (isset($_POST["user_to"])) {
            $user_to = $_POST["user_to"];
        }

        if (isset($_POST["message"])) {
            $message = $_POST["message"];
        }

        if (isset($_POST["attachment"])) {
            $attachment = $_POST["attachment"];
        }

        $created = @date('Y-m-d H:i:s');

        $query = $dbconn->prepare("insert into conversations(user_from, user_to, message, attachment, created)
                                            values(:user_from, :user_to, :message, :attachment, :created)");

        $query->bindParam(':user_from', $user_from);
        $query->bindParam(':user_to', $user_to);
        $query->bindParam(':message', $message);
        $query->bindParam(':attachment', $attachment);
        $query->bindParam(':created', $created);

        if ($query->execute()) {
            $arr = array(
                'isSuccess' => true,
                'userFrom' => $user_from,
                'userTo' => $user_to,
                'message' => $message,
                'attachment' => $attachment,
                'created' => $created,
            );

        } else {
            $arr = array(
                'isSuccess' => false,
                'message' => $query->errorInfo()
            );
        }

        echo json_encode($arr);

        break;
    }
    case "retrieveConversation": {
        if (isset($_GET["user_from"])) {
            $user_from = $_GET["user_from"];
        }

        if (isset($_GET["user_to"])) {
            $user_to = $_GET["user_to"];
        }

        $query = $dbconn->prepare("select * from conversations
                                        where (user_from=:userOne and user_to=:userTwo) 
                                        or user_from=:userTwo and user_to=:userOne");

        $query->bindParam(':userOne', $user_from);
        $query->bindParam(':userTwo', $user_to);

        if ($query->execute()) {
            echo json_encode($query->fetchAll());
        } else {
            echo $query->errorInfo();
        }

        break;
    }
    case "logout": {
        session_destroy();

        echo json_encode(array(
           "isSuccess" => true
        ));

        break;
    }
    case "recoverPassword": {
        if (isset($_POST["username"])) {
            $username = $_POST["username"];
        }

        $newPassword = rand_string(10); // it should be random string

        $query = $dbconn->prepare("update users set user_password=:userPassword
                                            where user_email=:userEmail");

        $query->bindParam(':userEmail', $username);
        $query->bindParam(':userPassword', $newPassword);

        if ($query->execute()) {
            $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
            try {
                //Server settings
                $mail->SMTPDebug = 2;                                 // Enable verbose debug output
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'nguyenvuphattdt@gmail.com';                 // SMTP username
                $mail->Password = 'trucphuong';                           // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                    // TCP port to connect to

                //Recipients
                $mail->setFrom('nguyenvuphattdt@gmail.com');
                $mail->addAddress('trucphuongup@gmail.com');     // Add a recipient

                //Content
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = 'New your password from trucphuong-1711217 company';
                $mail->Body    = 'Your password is: '.$newPassword;

                $mail->send();
                echo 'Message has been sent';

            } catch (Exception $e) {
                echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            }
        }

        break;
    }
}

function rand_string($length) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    return substr(str_shuffle($chars),0, $length);

}
