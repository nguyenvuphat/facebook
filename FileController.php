<?php
if(isset($_FILES["file"]["type"]))
{
    $validExtensions = ['pdf', 'txt', 'doc', 'docx', 'png', 'jpg', 'jpeg',  'gif'];

    $file_extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

    if (in_array($file_extension, $validExtensions)) {
        if (file_exists("uploads/" . $_FILES["file"]["name"])) {
            echo $_FILES["file"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
        }
        else
        {
            $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
            $targetPath = "uploads/".$_FILES['file']['name']; // Target path where file is to be stored
            move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file

            echo json_encode(array(
                "path" => $targetPath,
                "extension" => $file_extension
            ));
        }
    }
    else
    {
        echo "<span id='invalid'>***Invalid file Size or Type***<span>";
    }

}
?>
