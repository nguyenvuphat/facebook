<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Facebook Login Page</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="login-wrapper">
    <div class="login-wrapper__header">
        <div class="container header-box">
            <a href="#" class="login-brand">facebook</a>
            <div class="login-form">
                <div class="login-form__email">
                    <label class="title">Email or phone</label>
                    <input class="input-login" name="username" type="email" id="username">
                </div>
                <div class="login-form__password">
                    <label class="title">Password</label>
                    <input class="input-login" name="password" type="password" id="password">
                    <a href="#" class="forgot-password-link">Forgotten account?</a>
                </div>
                <input type="submit" class="btn-login" value="Login" onclick="login()">

            </div>
        </div>
    </div>
    <div class="login-wrapper__content">
        <div class="container content-box">
            <div class="content-left">
                <span class="image-description">Facebook helps you connect and share with the people in your life.</span>
                <img src="https://static.xx.fbcdn.net/rsrc.php/v3/yi/r/OBaVg52wtTZ.png">
            </div>
            <div class="content-right">
                <h1 class="register-title">Create an account</h1>
                <div class="account-info">
                    <label class="info-label">Account info</label>
                    <div class="info-input">
                        <div class="info-name">
                            <input class="account-input" type="text" name="firstName" placeholder="First name">
                            <input class="account-input" type="text" name="lastName" placeholder="lastName">
                        </div>
                        <input type="text" name="emailUser" placeholder="Mobile number or email address" class="account-input">
                        <input type="password" name="newPassword" placeholder="New password" class="account-input">
                    </div>
                    <label class="info-label">Date of birth</label>
                    <div class="user-birthday">
                        <select class="day">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                        </select>
                        <select class="month">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                        <select class="year">
                            <option>2000</option>
                            <option>1999</option>
                            <option>1998</option>
                            <option>1997</option>
                            <option>1996</option>
                            <option>1995</option>
                            <option>1994</option>
                        </select>
                    </div>
                    <label class="info-label">Gender</label>
                    <div class="user-gender">
                        <input type="radio" id="male" name="gender" value="male" checked>
                        <label for="male">Male</label>
                        <input type="radio" id="female" name="gender" value="female">
                        <label for="female">Female</label>
                    </div>
                </div>
                <a id="btn-sign-up" class="btn-sign-up">Sign Up</a>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</body>

<script>
    $("#btn-sign-up").click(function () {
        registerUser();
    })

    function registerUser() {
        var user = {
            action: 'add',
            firstName: $('input[name = "firstName"]').val(),/*firstName cua the input*/
            lastName: $('input[name = "lastName"]').val(),
            email: $('input[name = "emailUser"]').val(),
            password: $('input[name = "newPassword"]').val(),
            gender: $("input[name='gender']:checked").val(),
            birthDay: function(){
                var selectedDay = $('.day option:selected').text();
                var selectedMonth = $('.month option:selected').text();
                var selectedYear = $('.year option:selected').text();

                var current_datetime = new Date(selectedYear, selectedMonth, selectedDay, 0, 0, 0, 0, 0);
                return current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate()
            }()
        }

        console.log(user);

        $.ajax({
            url: "controller/UserController.php",
            type: "POST",
            data: user,
            success: function (data)
            {
                console.log(data);
            }
        });
    }

    function login() {
        var username = $('#username').val();
        var password = $('#password').val();
        $.ajax({
            type: 'POST',
            url: 'controller/UserController.php',
            data: {
                username: username,
                password: password,
                action: "login"
            },
            success: function (message) {
                if (message.trim() === "Login successfully") {
                    window.location = "home.php";
                }
                else {
                    alert(message);
                }
            }
        });
    }
</script>

</html>