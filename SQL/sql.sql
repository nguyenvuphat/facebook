CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_firstname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_lastname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_gender` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_birthday` DATE COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_email` (`user_email`);
  
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Create table post --

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `post_message` text NOT NULL,
  `post_image` text NULL,
  `poster` int(11) NOT NULL,
  `created` datetime NOT NULL
)

ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
  ADD CONSTRAINT `post_fk_user_id_1` FOREIGN KEY (`poster`) REFERENCES `users` (`id`);

-- create table friend_requests --

CREATE TABLE `friend_requests` (
  `id` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL
)

ALTER TABLE `friend_requests`
  ADD PRIMARY KEY (`id`),
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
  ADD CONSTRAINT `friend_request_ibfk_1` FOREIGN KEY (`sender`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `friend_request_ibfk_2` FOREIGN KEY (`receiver`) REFERENCES `users` (`id`);

-- create table friends --

CREATE TABLE `friends` (
  `id` int(11) NOT NULL,
  `userOne` int(11) NOT NULL,
  `userTwo` int(11) NOT NULL
)

ALTER TABLE `friends`
  ADD PRIMARY KEY (`id`),
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
  ADD CONSTRAINT `friend_fk_1` FOREIGN KEY (`userOne`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `friend_fk_2` FOREIGN KEY (`userTwo`) REFERENCES `users` (`id`);

-- create table conversation --

CREATE TABLE `conversations` (
  `id` int(11) NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `message` varchar(200) NOT NULL,
  `attachment` varchar(100) NOT NULL,
  `created` datetime NOT NULL
)

ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`),
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
  ADD CONSTRAINT `conversations_fk_1` FOREIGN KEY (`user_from`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `conversations_fk_2` FOREIGN KEY (`user_to`) REFERENCES `users` (`id`);


